"add tabs as spaces
set autoindent
set smartindent
set smarttab
set shiftwidth=4
set softtabstop=4
set tabstop=4
set expandtab
set cursorline

set ignorecase
set smartcase

"Respect Wraps
nnoremap k gk
nnoremap j gj
nnoremap gk k
nnoremap gj j

" Visual mode pressing * or # searches for the current selection
" Super useful! From an idea by Michael Naumann
vnoremap <silent> * :<C-u>call VisualSelection('', '')<CR>/<C-R>=@/<CR><CR>
vnoremap <silent> # :<C-u>call VisualSelection('', '')<CR>?<C-R>=@/<CR><CR>

"Always show current position
set ruler

" Turn backup off, since most stuff is in SVN, git et.c anyway...
set nobackup
set nowb
set noswapfile

"Speed things up a little
set lazyredraw

" Nvim sets terminal cursor on exit
autocmd VimLeave * set guicursor=a:hor1-blinkon1

filetype plugin on
filetype indent on

"SETUP
set visualbell                      "No sounds
set autoread                        "Reload files changed outside vim
set laststatus=2                    "Enabling statusline at all times
if &encoding != 'utf-8'
    set encoding=utf-8              "Necessary to show Unicode glyphs
endif
set noshowmode                      "Don't show the mode(airline is handling this)

imap jj <Esc>
let mapleader = "\<Space>"
" set cursorline                      "Highlight the line the cursor is on, has bad performance
set number                          "Line numbers


"Python
let g:python3_host_prog = '~/.local/apps/pyenv/versions/neovim3/bin/python'

"RG: add Rg command for ripgrep
command! -bang -nargs=* Rg
            \ call fzf#vim#grep(
            \   'rg --smart-case --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
            \   <bang>0 ? fzf#vim#with_preview('up:60%')
            \           : fzf#vim#with_preview('right:50%:hidden', '?'),
            \ <bang>0)
:map <Leader>r :Rg 

call plug#begin()
"TPOPE
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-surround'

"Terminal
Plug 'wincent/terminus'

" Linting
Plug 'w0rp/ale'

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'easymotion/vim-easymotion'

"Airine
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Autoformat
Plug 'chiel92/vim-autoformat'
Plug 'jiangmiao/auto-pairs'
"Autocompletion
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'zchee/deoplete-clang'
Plug 'ujihisa/neco-look'
Plug 'autozimu/LanguageClient-neovim', {
            \ 'branch': 'next',
            \ 'do': 'bash install.sh',
            \ }

"Snippets (uses deoplete)
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/neosnippet-snippets'

"Style
Plug 'joshdick/onedark.vim'
Plug 'fenetikm/falcon'
Plug 'ayu-theme/ayu-vim'
Plug 'rakr/vim-two-firewatch'
Plug 'airblade/vim-gitgutter'
Plug 'junegunn/vim-emoji'

"Langs
Plug 'sheerun/vim-polyglot'
Plug 'lervag/vimtex'
Plug 'let-def/vimbufsync'
Plug 'tmux-plugins/vim-tmux'

"Gui
"Plug 'equalsraf/neovim-gui-shim'
call plug#end()

"Setting the colorscheme
if &t_Co >= 256 || has("gui_running")
    set termguicolors
    set background=dark" or light if you prefer the light version
    let ayucolor="dark"
    colorscheme ayu
endif
if &t_Co > 2 || has("gui_running")
    "switch syntax highlighting on, when the terminal has colors
    syntax on
endif

" Polyglot
let g:polyglot_disabled = ['latex'] "Handled by vimtex

"Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

"FZF
function! s:fzf_statusline()
    " Override statusline as you like
    highlight fzf1 ctermfg=161 ctermbg=251
    highlight fzf2 ctermfg=23 ctermbg=251
    highlight fzf3 ctermfg=237 ctermbg=251
    setlocal statusline=%#fzf1#\ >\ %#fzf2#fz%#fzf3#f
endfunction
:map <c-p> :GFiles<cr>
:map <Leader>b :Buffers<cr>
:map <Leader>m :Marks<cr>
:map <Leader>f :Files<cr>

nmap <Leader>ha <Plug>GitGutterStageHunk
nmap <Leader>hr <Plug>GitGutterUndoHunk

autocmd! User FzfStatusLine call <SID>fzf_statusline()

"Airline
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#formatter='unique_tail_improved'
let g:airline#extensions#tabline#enabled = 1
" let g:falcon_airline = 1
let g:airline_theme='twofirewatch'
let g:two_firewatch_italics=1

"Ale
let g:airline#extensions#ale#enabled = 1
let g:ale_linters = {
            \   'c': ['clang-format', 'clang'],
            \   'cpp': ['clang-format', 'clang'],
            \   'javascript': ['standard'],
            \   'python': ['mypy', 'flake8'],
            \}

"Deoplete
let g:deoplete#enable_at_startup = 1
inoremap <expr> <CR> (pumvisible() ? "\<c-y>\<cr>" : "\<CR>")
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
set completeopt-=preview
" Use smartcase.
let g:deoplete#enable_smart_case = 1

"Language Server
let g:LanguageClient_completionPreferTextEdit=1
nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <Leader>s :call LanguageClient#textDocument_documentSymbol()<CR>
let g:LanguageClient_serverCommands = {
            \ 'cpp': ['/home/bjudd/.local/apps/clang+llvm-8/clang+llvm-8.0.0-x86_64-linux-gnu-ubuntu-16.04/bin/clangd'],
            \ 'c': ['/home/bjudd/.local/apps/clang+llvm-8/clang+llvm-8.0.0-x86_64-linux-gnu-ubuntu-16.04/bin/clangd'],
            \ 'python': ['~/.local/bin/pyls'],
            \ 'java': ['/usr/bin/jdtls'],
            \ }
" \ 'rust': ['~/.cargo/bin/rustup', 'run', 'stable', 'rls'],
" \ 'javascript': ['/usr/local/bin/javascript-typescript-stdio'],
" \ 'javascript.jsx': ['tcp://127.0.0.1:2089'],


"NeoSnippet
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
imap <C-k>     <Plug>(neosnippet_expand_or_jump)
smap <C-k>     <Plug>(neosnippet_expand_or_jump)
xmap <C-k>     <Plug>(neosnippet_expand_target)


" <C-h>, <BS>: close popup and delete backword char.
inoremap <expr><C-h> deoplete#smart_close_popup()."\<C-h>"
inoremap <expr><BS>  deoplete#smart_close_popup()."\<C-h>"

" <CR>: close popup and save indent.
inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
function! s:my_cr_function() abort
    return deoplete#close_popup() . "\<CR>"
endfunction

"AutoFormat
noremap <C-I> :Autoformat<CR>

"Vimtex
let g:tex_flavor='latex'
"Makes syntax highlighting faster for latex at the loss of some features
" let g:tex_fast='M'
"let g:neotex_latexdiff=1
let g:vimtex_view_method='zathura'
let g:vimtex_compiler_progname = 'nvr'
let g:vimtex_imaps_leader='@'
" This is new style
let g:vimtex_quickfix_latexlog = {
            \ 'default' : 1,
            \ 'general' : 1,
            \ 'references' : 1,
            \ 'overfull' : 0,
            \ 'underfull' : 0,
            \ 'font' : 1,
            \ 'packages' : {
            \   'default' : 1,
            \   'natbib' : 1,
            \   'biblatex' : 1,
            \   'babel' : 1,
            \   'hyperref' : 1,
            \   'scrreprt' : 1,
            \   'fixltx2e' : 1,
            \   'titlesec' : 1,
            \ },
            \}

"Git gutter
let g:gitgutter_realtime = 1
set updatetime=500
" From vim-emoji
" let g:gitgutter_sign_added = emoji#for('small_blue_diamond')
" let g:gitgutter_sign_modified = emoji#for('small_orange_diamond')
" let g:gitgutter_sign_removed = emoji#for('small_red_triangle')
" let g:gitgutter_sign_modified_removed = emoji#for('collision')
"
let g:gitgutter_sign_added = "\uF893"
let g:gitgutter_sign_modified = "\uF459"
let g:gitgutter_sign_removed = "\uFBCA"
let g:gitgutter_sign_modified_removed = "\uFB14"

" Functions
function! VisualSelection(direction, extra_filter) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", "\\/.*'$^~[]")
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'gv'
        call CmdLine("Ack '" . l:pattern . "' " )
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction
