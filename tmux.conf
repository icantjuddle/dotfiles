set-option -g prefix C-a
unbind C-b
bind-key C-a send-prefix

#Fix slow escape key
set -sg escape-time 0

# Set a Ctrl-b shortcut for reloading your tmux config
bind r source-file ~/.tmux.conf

set-option -g mouse on
set -g set-titles on
set -g default-shell /usr/bin/zsh
set -g default-terminal "tmux-256color"
set -ga terminal-overrides ",*col*:Tc"

# Vim style keybindings
set-option -wg mode-keys vi
unbind-key j
bind-key j select-pane -D
unbind-key k
bind-key k select-pane -U
unbind-key h
bind-key h select-pane -L
unbind-key l
bind-key l select-pane -R

#Start numbering at 1
set -g base-index 1
set-option -wg pane-base-index 1

# Allow using FocusIn and FocusOut hooks 
# Used by kakoune greying of inactive pane
set-option -g focus-events on

######################
### DESIGN CHANGES ###
######################

# messaging
set-option  -g message-style bold
set-option -ag message-style fg=colour232
set-option -ag message-style bg=colour166
set-option  -g message-command-style fg=blue
set-option -ag message-command-style bg=black

# loud or quiet?
set-option -g visual-activity off
set-option -g visual-bell off
set-option -g visual-silence off
set-option -wg monitor-activity off
set-option -g bell-action none


# The modes
set-option -wg clock-mode-colour colour135
set-option  -wg mode-style bold
set-option -awg mode-style fg=colour196
set-option -awg mode-style bg=colour238

# The panes

set-option -g pane-border-style bg=default #colour235
set-option -ag pane-border-style fg=colour238
set-option -g pane-active-border-style bg=colour67 #colour236
set-option -ag pane-active-border-style fg=colour238 #51
set-option -ag pane-active-border-style bold

# The statusbar

set-option -g status-position bottom
set-option -g status-justify left
set -g status-interval 2
set-option -ag status-style bg=colour230 #234
set-option -ag status-style fg=colour137
set-option -ag status-style dim
set-option -g status-left ''
set-option -g status-left-length 20
set-option -g status-right '#[fg=colour233,bg=colour241] #S #[fg=colour233,bg=colour245] %r'
set-option -g status-right-length 50


set-option -wg window-status-current-style fg=colour81
set-option -awg window-status-current-style bg=colour238
set-option -awg window-status-current-style none
set-option -wg window-status-current-format ' #I#[fg=colour250]:#[fg=colour255]#W#[fg=colour50]#F '

set-option -wg window-status-style fg=black #colour138
set-option -awg window-status-style bg=colour230 #235
set-option -awg window-status-style none
set-option -wg window-status-format ' #I:#W#[fg=colour244]#F '
# set-option -wg window-status-format ' #I#[fg=colour237]:#[fg=colour250]#W#[fg=colour244]#F '

set-option -wg window-status-bell-style bold
set-option -awg window-status-bell-style fg=colour255
set-option -awg window-status-bell-style bg=colour1

set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'nhdaly/tmux-better-mouse-mode'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'fcsonline/tmux-thumbs'

set -g @continuum-restore 'on'

#initialize TPM (has to be at bottom of .tmux.conf)
run '~/.tmux/plugins/tpm/tpm'
