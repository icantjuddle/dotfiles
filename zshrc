# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

# Keybinds
function zvm_after_init() {
  bindkey '^E' end-of-line                            # [Ctrl-E] - Jump to the end of the line
  bindkey '^[[P' delete-char                          # [Delete] - Delete character under cursor
  bindkey '^[[4~' end-of-line                         # [End] - Go to end of line
  bindkey '^[[3~' delete-char                         # [Delete] - Delete character under cursor
  bindkey '^A' beginning-of-line                      # [Ctrl-A] - Jump to the start of the line
  bindkey '^[[1;5C' forward-word                      # [Ctrl-RightArrow] - move forward one word
  bindkey '^[[1;5D' backward-word                     # [Ctrl-LeftArrow] - move backward one word
  bindkey '^[[H' beginning-of-line                    # [Home] - Go to beginning of line
  bindkey '^?' backward-delete-char                   # [Backspace] - Delete backward
  bindkey '^[[5~' up-line-or-history                  # [PageUp] - Up a line of history
  bindkey '^[[6~' down-line-or-history                # [PageDown] - Down a line of history
  bindkey '^[[Z' reverse-menu-complete                # [Shift-Tab] - move through the completion menu backwards
  bindkey '^H' backward-kill-word                     # [Ctrl-Backspace] - Delete previous work
  # bindkey '^r' history-incremental-search-backward  # [Ctrl-r] - Search backward incrementally for a string.
  bindkey '^r' skim-history-widget
  bindkey '^t' skim-file-widget                       # [Ctrl-t] - fuzzy find a file.
  bindkey '^b' skim-branch-widget                     # [Ctrl-b] - fuzzy find a git branch.
  bindkey '^ ' autosuggest-accept
  bindkey '^e' autosuggest-execute
}

# History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000
setopt appendhistory
setopt incappendhistory
setopt sharehistory

setopt autocd
zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#) ([0-9a-z-]#)*=01;34=0=01'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*:functions' ignored-patterns '(_*|pre(cmd|exec))'
zstyle ':completion:*' menu select

# export TERM="xterm-256color"

function __cbr() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  git branch --color -lvv \
    | $(__skimcmd) --border --ansi --reverse \
      --preview 'git tree --first-parent --color=always -n 10 "$(sed -En "s,\W?\s+\b(\S+)\b.*,\1,p" <<< {})"' \
    | sed -En 's,\W?\s+\b(\S+)\b.*,\1,p'
}

skim-branch-widget() {
  LBUFFER="${LBUFFER} $(__cbr)"
  local ret=$?
  # zle reset-prompt
  return $ret
}
zle -N skim-branch-widget

function __cbrs() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  git branch --color -lvv \
    | sk --border --ansi --height 25 --multi \
      --preview 'git tree --first-parent --color=always -n 10 "$(sed -En "s,\W?\s+\b(\S+)\b.*,\1,p" <<< {})"' \
    | sed -En 's,\W?\s+\b(\S+)\b.*,\1,p'
}

function __cwt() {
  local -r wt="$(git worktree list | fzf --height 15 --border --ansi | cut -d " " -f 1)"
  if [[ -n "$wt" ]]; then
    basename "$wt"
  else
    printf ""
  fi
}

function gwt-remove() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  local -r worktree="$(__cwt)"
  local -r ret=$?
  if [[ -n "$worktree" ]]; then
    git worktree remove "$worktree"
  fi
  return $ret
}
zle -N gwt-remove

function gb-del() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  local -ar branches=("${(@f)$(__cbrs)}")
  local -r ret=$?
  for b in $branches; do
    git branch -D "$b"
  done
}
zle -N gb-del

gbc() {
  setopt localoptions pipefail no_aliases 2> /dev/null
  local -r branch=$(__cbr)
  local -r ret=$?
  if [[ -n $branch ]]; then
    git switch "$branch"
  fi
  return $ret
}
zle -N gbc

# Speed these up by overriding find with fd
SKIM_CTRL_T_COMMAND="fd -Luu . --min-depth 1  -t f -t d -t l '.'"
SKIM_ALT_C_COMMAND="fd -Luu . --min-depth 1  -t d '.'"

function ppgrep() {
  pgrep "$@" | xargs --no-run-if-empty ps fp
}

alias ls="exa"
alias ll="exa --long --git"
alias la="exa --long --git --all"

#Add local shell scripts
alias ]="open"

source ~/.config/broot/launcher/bash/br
  
ZSH_AUTOSUGGEST_USE_ASYNC=1
ZSH_AUTOSUGGEST_STRATEGY=(history completion)
function __zsh_autosuggest_bind() {
}

function zvm_config () {
  ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT
  ZVM_NORMAL_MODE_CURSOR=$ZVM_CURSOR_UNDERLINE
  ZVM_INSERT_MODE_CURSOR=$ZVM_CURSOR_BEAM
  ZVM_VISUAL_MODE_CURSOR=$ZVM_CURSOR_BLINKING_BLOCK
  ZVM_VISUAL_LINE_MODE_CURSOR=$ZVM_CURSOR_BLINKING_BLOCK
}

source ~/.zinit/bin/zinit.zsh

zinit light willghatch/zsh-saneopt

# Fast-syntax-highlighting & autosuggestions
zinit wait lucid for \
 atinit"ZINIT[COMPINIT_OPTS]=-C; zpcompinit; zpcdreplay" \
    zdharma-continuum/fast-syntax-highlighting \
 atload"!__zsh_autosuggest_bind;" \
    zsh-users/zsh-autosuggestions \
 blockf atpull'zinit creinstall -q .' \
    zsh-users/zsh-completions

zinit ice atclone'PYENV_ROOT="$PWD" ./libexec/pyenv init - | grep -v "pyenv rehash" > zpyenv.zsh && git clone https://github.com/pyenv/pyenv-virtualenv ./plugins/pyenv-virtualenv && ./libexec/pyenv virtualenv-init - >> zpyenv.zsh' \
      atinit'export PYENV_ROOT="$PWD"' \
      atpull"%atclone" \
      src'zpyenv.zsh' \
      wait lucid
zinit light pyenv/pyenv

zinit ice lucid silent wait'1' id-as"sk-key-bindings" pick"shell/key-bindings.zsh"
zinit light lotabout/skim

zinit ice silent depth=1 wait''
zinit light jeffreytse/zsh-vi-mode

# zinit ice depth=1
# zinit light romkatv/powerlevel10k

zinit ice compile'(pure|async).zsh' pick'async.zsh' src'pure.zsh'
zinit light sindresorhus/pure

# # To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
# [[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
