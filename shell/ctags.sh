#!/bin/bash

set -euo pipefail

declare -ar CTAGS_ARGS=(
  "-R"
  "--exclude=bazel-*"
  "--exclude=.git"
  "--exclude=node_modules"
  "--links=no"
  "--fields=+iaS"
  "--extras=+q"
  "--languages=all"
  "--languages=-JavaScript"
  "--languages=-TypeScript"
)

ctags "${CTAGS_ARGS[@]}"
