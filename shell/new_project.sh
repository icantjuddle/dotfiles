#!/bin/bash

set -euo pipefail

if [[ "$#" -lt 2 ]]; then
  echo "Usage new_project.sh branch_name, project_name [opt:base_branch]"
  exit 1
fi
readonly BNAME="$1"
readonly PNAME="$2"
if [[ "$#" -eq 3 ]]; then
  readonly BASE="$3"
else
  readonly BASE="origin/green/simtests"
fi

pushd "$HOME/code/Nuro" >>/dev/null
git fetch --prune
git worktree add "$HOME/code/$BNAME" -b "bjudd/$BNAME" --no-checkout "$BASE"
mkdir ".git/worktrees/$BNAME/info/"

pushd "$HOME/code/$BNAME" >>/dev/null
git sparse-checkout init
git sparse-checkout set '/*' '!onboard/models/caffe/' '!onboard/models/tensorflow/' '!misc/scripts/bos/' '!docker/debs/' '!learning/nuroflow/'
git read-tree -mu HEAD

echo "nuro-toolchain" >> .python-version
ln -s ../Nuro/pyproject.toml ./

if ctags.sh &>/dev/null; then
  notify-send "CTags ready for $BNAME"
else
  notify-send "CTags failed for $BNAME"
fi

mkdir "project_local/"
cp -r ../common/* "$HOME/code/$BNAME/project_local/"

tmux new-session -c "$HOME/code/$BNAME" -s "$PNAME" -d
tmux new-window -t "$PNAME:1" -n "Code" -c "$HOME/code/$BNAME" -dk
tmux new-window -t "$PNAME:2" -n "Util" -c "$HOME/code/$BNAME" -dk
tmux new-window -t "$PNAME:3" -n "git" -c "$HOME/code/$BNAME" -dk

popd >>/dev/null
popd >>/dev/null
